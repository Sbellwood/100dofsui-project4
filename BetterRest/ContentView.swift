//
//  ContentView.swift
//  BetterRest
//
//  Created by Skyler Bellwood on 7/5/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    static var defaultWakeTime: Date {
        var components = DateComponents()
        components.hour = 7
        components.minute = 0
        return Calendar.current.date(from: components) ?? Date()
    }
    
    @State private var wakeUp = defaultWakeTime
    @State private var sleepAmount = 8.0
    @State private var coffeeAmount = 1
    
    
    var body: some View {
        NavigationView {
            VStack {
                Form {
                    Section(header:
                        Text("When do you want to wake up?")
                            .font(.headline)) {
                                DatePicker("Please enter a time", selection: $wakeUp, displayedComponents: .hourAndMinute)
                                    .labelsHidden()
                    }
                    Section(header:
                        Text("Desired amount of sleep")
                            .font(.headline)) {
                                Stepper(value: $sleepAmount, in: 4...12, step: 0.25) {
                                    Text("\(sleepAmount, specifier: "%g") hours")
                                }
                    }
                    Section(header: Text("Daily coffee intake")
                        .font(.headline)) {
                            //                        Picker(selection: $coffeeAmount, label: Text("Number of cups")) {
                            //                            ForEach(1 ..< 20) {
                            //                                if ($0 == 1) {
                            //                                    Text("1 cup")
                            //                                } else {
                            //                                    Text("\($0) cups")
                            //                                }
                            //                            }
                            //                        }
                            Stepper(value: $coffeeAmount, in: 1...20) {
                                if coffeeAmount == 1 {
                                    Text("1 cup")
                                } else {
                                    Text("\(coffeeAmount) cups")
                                }
                            }
                    }
                }
                Text("Ideal Bedtime")
                    .font(.headline)
                Text(calculateBedtime())
                    .font(.largeTitle)
            }
            .navigationBarTitle("BetterRest")
        }
    }
    
    func calculateBedtime() -> String {
        let model = SleepCalculator()
        let components = Calendar.current.dateComponents([.hour, .minute], from: wakeUp)
        let hour = (components.hour ?? 0) * 60 * 60
        let minute = (components.minute ?? 0) * 60
        
        do {
            let prediction = try model.prediction(wake: Double(hour + minute), estimatedSleep: sleepAmount, coffee: Double(coffeeAmount))
            let sleepTime = wakeUp - prediction.actualSleep
            
            let formatter = DateFormatter()
            formatter.timeStyle = .short
            
            return formatter.string(from: sleepTime)
            
        } catch {
            return "Sorry, there was a problem calculating your bedtime."
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
